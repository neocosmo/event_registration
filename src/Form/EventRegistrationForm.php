<?php

namespace Drupal\event_registration\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for registering to events.
 */
class EventRegistrationForm extends FormBase {

  /**
   * Messenger interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Time interface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('messenger'),
      $container->get('datetime.time')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(MessengerInterface $messenger, TimeInterface $time) {
    $this->messenger = $messenger;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'event_registration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $nid = 0) {
    $form['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h4',
      '#value' => $this->t('Registration'),
      '#attributes' => [
        'class' => ['form-title', 'event-registration-form-title'],
      ],
    ];
    $form['firstname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firstname'),
      '#description' => $this->t('Please provide your firstname.'),
      '#required' => TRUE,
    ];
    $form['lastname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lastname'),
      '#description' => $this->t('Please provide your lastname.'),
      '#required' => TRUE,
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#description' => $this->t('Please provide your e-mail.'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $form['nid'] = [
      '#type' => 'value',
      '#value' => $nid,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*
     * The only thing we want to validate here is, that there does not already
     * exist a registration for that particular event with the provided e-mail
     * address.
     */
    $values = $form_state->getValues();
    $email = $values['email'] ?? '';
    if (empty($email)) {
      $form_state->setErrorByName('email', 'You need to provide an e-mail address.');
      return;
    }
    $nid = $values['nid'] ?? 0;
    if (empty($nid)) {
      $form_state->setErrorByName('nid', 'Invalid form data. Please reload the page. Should this error persist, please contact the site administrator.');
      return;
    }
    $exists = (bool) db_select('event_registration_registrations', 'err')
      ->fields('err')
      ->condition('email', $email, '=')
      ->condition('nid', $nid, '=')
      ->countQuery()
      ->execute()
      ->fetchField();
    if ($exists) {
      $form_state->setErrorByName('email', 'The e-mail address is already used for registration.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $known_keys = [
      'firstname',
      'lastname',
      'email',
      'submit',
      'nid',
      'form_build_id',
      'form_token',
      'form_id',
      'op',
    ];
    $auxiliary_data = [];
    foreach ($values as $key => $value) {
      if (in_array($key, $known_keys)) {
        continue;
      }
      $auxiliary_data[$key] = $value;
    }
    db_insert('event_registration_registrations')
      ->fields([
        'nid' => $values['nid'],
        'email' => $values['email'],
        'firstname' => $values['firstname'],
        'lastname' => $values['lastname'],
        'timestamp' => $this->time->getRequestTime(),
        'auxiliary' => serialize($auxiliary_data),
      ])
      ->execute();
    $this->messenger->addMessage($this->t('You successfully registered for the event.'));
  }

}
